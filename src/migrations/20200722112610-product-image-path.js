"use strict";

const { QueryInterface, Sequelize, DataTypes } = require("sequelize");

module.exports = {
  /**
   * @param {QueryInterface} queryInterface
   * @param {Sequelize} Sequelize
   */
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn("Products", "imagePath", {
      type: DataTypes.STRING,
      allowNull: true,
    });
  },

  /** 
   * @param {QueryInterface} queryInterface
   */
  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn("Products", "imagePath")
  }
};
