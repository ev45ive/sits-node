import { Router, Express} from "express";
import { productsRoutes } from "./products";
// import { cartRoutes } from "./cart";
import { usersRoutes, UsersController } from "./users";

// /src/routes/api

export const apiRoutes = Router();

apiRoutes
  .use('/products', productsRoutes)
  // .use('/cart', cartRoutes)
  // .use('/users', usersRoutes)
  .use('/users', new UsersController().router)


  .get('/', (req, res) => {
    res.send('api')
  })


