import express, { Router } from "express";
import { OK, CREATED, NOT_FOUND, } from 'http-status-codes'
import { getAllProducts, getProductById, createProduct, productImageUpload } from "../../services/products";

// /src/routes/api/products
export const productsRoutes = Router();

productsRoutes

  .use('images', express.static('uploads/products'))

  /**
   * List of Products
   */
  .get('/', async (req, res) => {
    try {
      const filter = String(req.query['filter'] || '')
      const products = await getAllProducts({ filter })

      res.send(products)
    } catch (e) {
      res.status(500).send('Error')
    }
  })

  /**
   * One product
   */
  .get('/:product_id', async (req, res) => {
    const {
      product_id,
    } = req.params
    const product = await getProductById(product_id);

    if (!product) {
      res.status(NOT_FOUND).send('Not Found')
    }

    res.send(product)
  })

  .post('/', productImageUpload(), async (req, res) => {
    const { name, price } = req.body

    const product = await createProduct({ name, price, imageFile: req.file })

    res.status(CREATED).send(product)
  })

  .put('/:product_id', (req, res) => {
    res.status(OK).send('Updated')
  })