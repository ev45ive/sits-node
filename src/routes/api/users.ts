import { Router, Response, Request, response } from "express";
import { UsersService } from "../../services/users";
import Joi from "@hapi/joi";
import validate from 'express-joi-validation'
import { NOT_FOUND } from "http-status-codes";
import { User } from "../../model/user";

export const usersRoutes = Router();

// usersRoutes.get('/', (req, res) => {
//   res.send('users')
// })

export const usersService = new UsersService()

interface DataRequest extends Request {
  // data: { [k: string]: any }
  data: { user: User }
}

export class UsersController {

  router = Router();
  validator = validate.createValidator({})
  usersService = usersService

  constructor() {

    this.router.param('user_id', this.extractUserFromParam)

    this.router.get('/', this.getUsers)
    this.router.get('/:user_id', this.getUser)
    this.router.post('/:id/activate', this.activateUser)

    this.router.put('/:id',
      this.validator.body(createUserSchema),
      this.updateUser
    )
    this.router.post('/',
      this.validator.body(createUserSchema),
      this.createUser
    )
  }

  getUser = (req: DataRequest, res: Response) => {
    res.send(req.data.user)
  }

  activateUser = async ({ params: { id } }: Request, res: Response) => {
    const updated = this.usersService.activateUser(id)
    res.send(updated)
  }

  updateUser = async ({ params: { id }, body }: Request, res: Response) => {
    const updated = await this.usersService.updateUser(id, body)
    res.send(updated)
  }

  getUsers = async (req: Request, res: Response) => {
    res.send(await this.usersService.getAll())
  }

  createUser = async (req: Request, res: Response) => {

    const createUserDTO = req.body;
    const user = await this.usersService.createUser(createUserDTO)
    res.send(user)
  }

  private extractUserFromParam = async (req, res, next, user_id) => {
    const user = await this.usersService.getById(user_id);
    if (user) {
      (req as any).data = { user };
      next();
    }
    else {
      res.status(NOT_FOUND).send({ error: 'User not found' });
    }
  }
}

export const createUserSchema = Joi.object({
  username: Joi.string()
    .alphanum()
    .min(3)
    .max(30)
    .required(),

  password: Joi.string()
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
    .required(),

  // repeat_password: Joi.ref('password'),

  birthday: Joi.date(),

  email: Joi.string().email()
    .required()
})