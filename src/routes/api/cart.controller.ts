import { Router} from "express";
import { getUserCart, addProductToCart, removeProductFromCart, resetCart } from "../../services/cart";
import { Route, Controller, Get, Path, SuccessResponse, Post, Body, Response, Query, } from 'tsoa';
import { UsersService } from "../../services/users";

@Route("api/v1/cart")
export class CartController extends Controller {

  @Get('/')
  async getAll(@Query() filter: string = '') {
    return [
      filter
    ]
  }

  @Get('{userId}')
  async get(@Path() userId: number) {
    return {
      items: [],
      total: 0
    }
  }

  @SuccessResponse("201", "Created") // Custom success response
  @Post()
  async addToCart(
    @Body() requestBody: AddToCartDTO,
    // @Request() request: Request,
    // @Response() response: Response,
  ) {

  }

}

export interface AddToCartDTO {
  product_id: number,
  amount: number
}

// export const cartRoutes = Router();

// cartRoutes.get('/', async (req, res) => {
//   const cart = await getUserCart({});

//   res.send(cart)
// })

//   .post('/add', async (req, res) => {
//     try {
//       await addProductToCart()

//       res.send('OK')
//     } catch (e) {/*  */ }
//   })

//   .post('/remove', async (req, res) => {
//     try {
//       await removeProductFromCart()

//       res.send('OK')
//     } catch (e) {/*  */ }
//   })

//   .post('/reset', async (req, res) => {
//     try {
//       await resetCart()

//       res.send('OK')
//     } catch (e) {/*  */ }

//   })