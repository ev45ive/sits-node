import { Router } from "express";
import { createProduct, productImageUpload } from "../services/products";
import passport from 'passport'
export const pages = Router();

pages.use((req, res, next) => {
  res.locals.title = "Express App"
  res.locals.session  = req.session
  res.locals.flash = req.session.flash
  res.locals.message = req.session.message
  
  // res.locals.success_messages = (req as any).flash('success_messages');
  // res.locals.error_messages = (req as any).flash('error_messages');
  // delete req.session.message

  next()
})

pages.get('/', (req, res) => {
  res.render('index', { message: 'Hello there!' })
})

pages.get('/about', (req, res) => {
  res.send('about')
})

pages.post('/products/create', productImageUpload(), async (req, res) => {
  const { name, price } = req.body
  const product = await createProduct({ name, price, imageFile: req.file })

  req.session.message = 'Product created';
  res.redirect('/products/create')
})

pages.get('/products/create', (req, res) => {
  res.render('products/create', { product: null })
})

pages.get('/login', (req, res) => {
  res.render('login')
})

pages.post('/login',
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: 'Failed to log in',
    successFlash: 'Sucesfully logged in',
  }));


pages.get('/visits', (req, res) => {
  // throw new Error('ups..')
  // res.send('api')

  if (req.session.views) {
    req.session.views++
    res.setHeader('Content-Type', 'text/html')
    res.write('<p>views: ' + req.session.views + '</p>')
    res.write('<p>expires in: ' + (req.session.cookie.maxAge / 1000) + 's</p>')
    res.end()
  } else {
    req.session.views = 1
    res.end('welcome to the session demo. refresh!')
  }

})


import express, { Response as ExResponse, Request as ExRequest } from "express";
import swaggerUi from "swagger-ui-express";

pages.use("/docs", swaggerUi.serve, async (_req: ExRequest, res: ExResponse) => {
  return res.send(
    swaggerUi.generateHTML(await import("../build/swagger.json"))
  );
});