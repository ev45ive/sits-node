import { Sequelize, Model, DataTypes } from 'sequelize'
import { sequelize } from '.'

export class User extends Model {
  username: string
  birthday?: string

  password: string
  email: string
  active: boolean
}

User.init({
  username: DataTypes.STRING,
  password: DataTypes.STRING,
  email: DataTypes.STRING,
  birthday: DataTypes.DATE,
  active: DataTypes.BOOLEAN
}, {
  sequelize, modelName: 'user', defaultScope: {
    where: {
      active: true
    },
    attributes: {
      exclude: ['password']
    }
  },
  scopes: {
    deleted: {
      where: {
        deleted: true
      }
    }
  }
});


export const populateUserTables = () => {
  return sequelize.sync()
    .then(() => User.create({
      username: 'janedoe',
      birthday: new Date(1980, 6, 20)
    }))
    .then(jane => {
      console.log(jane.toJSON());
    });
}
