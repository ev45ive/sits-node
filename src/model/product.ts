
import { sequelize } from '.'
import { Model, DataTypes } from "sequelize";

export class Product extends Model {
  name: string
  price: number
  description: string
  imagePath: string

  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
  }
};

Product.init({
  name: DataTypes.STRING,
  price: DataTypes.NUMBER,
  description: DataTypes.STRING,
  imagePath: DataTypes.STRING,

}, {
  sequelize,
  modelName: 'Product',
});
