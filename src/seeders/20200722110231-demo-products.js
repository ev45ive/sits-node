"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "Products",
      [
        {
          id: "1",
          name: "Product ABC",
          price: 100,
          description: " Some text ...",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: "12",
          name: "Product ABCD",
          price: 150,
          description: " Some text ...",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: "13",
          name: "Product ZXY",
          price: 200,
          description: " Some text ...",
          createdAt: new Date(),
          updatedAt: new Date()
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Products", null, {});
  },
};
