import { productsRoutes } from "../routes/api/products";
import uuid from 'short-uuid'

import fs from 'fs'

import multer from 'multer'
import path from 'path'
import { response } from "express";
import { Product } from "../model/product";

var upload = multer({ dest: 'uploads/products' })

export const productImageUpload = () => upload.single('image')



export const getAllProducts = async ({ filter }) => {
  return Product.findAll({})
}

export const getProductById = async (id) => {
  return Product.findByPk(id)
}

export const createProduct = async ({
  name, price, imageFile
}) => {
  const imagePath = path.join(imageFile.destination, imageFile.originalname)

  imageFile && await fs.promises.rename(
    path.join(imageFile.destination, imageFile.filename),
    imagePath
  )

  const product = new Product({
    name,
    price,
    imagePath
  });
  return product.save();
}

export const updateProduct = async () => { }