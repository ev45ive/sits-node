import { User } from "../model/user";
import crypto from 'crypto'

export class UsersService {

  async getAll() {
    return User.findAll()
  }

  async getById(id) {
    return User.findByPk(id)
  }

  async createUser(createUserDTO: CreateUserDTO) {
    // const u = new User()
    // u.save()

    const passwordHash = this.hashPassword(createUserDTO.password)

    const user = await User.create({
      ...createUserDTO,
      password: passwordHash,
      active: false
    });

    return user
  }
  async updateUser(id: string, updateUserDTO: UpdateUserDTO) {

    const user = await this.getById(id)

    const updated = user.update({
      ...updateUserDTO,
    });

    return updated
  }

  hashPassword(password: string) {
    return crypto.createHash('md5').update(password + 'salt123').digest("hex");
  }

  async activateUser(user_id: string) {
    const user = await User.findOne({ where: { id: user_id, active: false } })
    user.active = true;

    return user.save({})
  }
}

interface CreateUserDTO {
  username: string
  birthday: string
  password: string
  email: string
}
interface UpdateUserDTO {
  username?: string
  birthday?: string
  password?: string
  email?: string
}