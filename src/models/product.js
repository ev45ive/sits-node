"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.Product = void 0;
var _1 = require(".");
var types_1 = require("sequelize/types");
var Product = /** @class */ (function (_super) {
    __extends(Product, _super);
    function Product() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    Product.associate = function (models) {
        // define association here
    };
    return Product;
}(types_1.Model));
exports.Product = Product;
;
Product.init({
    name: types_1.DataTypes.STRING,
    price: types_1.DataTypes.NUMBER,
    description: types_1.DataTypes.STRING
}, {
    sequelize: _1.sequelize,
    modelName: 'Product'
});
