
import express from "express";
import cors from 'cors'
import session from 'express-session'
import timeout from 'connect-timeout'
import morgan from 'morgan'
import { passportLoader } from "./passport";
import flash from 'express-flash'
import cookieParser from 'cookie-parser'

export const init = async (app) => {
  app.set('views', './src/views')
  app.set('view engine', 'ejs')
  app.set("trust proxy", 1); // trust first proxy

  if (process.env.NODE_ENV == 'production') {
    app.use(timeout('3s'))
  }
  app.use(cors({}))
  app.use(morgan('dev', {
    // stream: whereTosteamLogs
  }))
  app.use(express.static('public', {
    /* cache options */
  }))
  app.use(cookieParser())
  app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
  }))
  app.use(flash())
  app.use(express.urlencoded({
    extended: true
  }))

  app.use(express.json({}))

  passportLoader(app)
}