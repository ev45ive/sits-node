import { Express } from 'express'
import { User } from '../model/user';
import { usersService } from '../routes/api/users';

export const passportLoader = (app: Express) => {

  var passport = require('passport')
    , LocalStrategy = require('passport-local').Strategy;

  passport.use(new LocalStrategy(
    async function (username, password, done) {

      const user = await User.findOne({
        where: {
          username: username,
          password: usersService.hashPassword(password)
        }
      })

      if (!user) {
        return done(null, false, { message: 'Incorrect username or password.' });
      }
      return done(null, user);

    }
  ));

  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(async function (id, done) {
    const user = await User.findByPk(id);
    done(null, user);
  });


  app.use(passport.initialize());
  app.use(passport.session());
}