// const dotenv = require("dotenv");
import dotenv from "dotenv";
dotenv.config();

// const http = require("http");
import express from "express";
import errorhandler from 'errorhandler'

import { apiRoutes } from "./routes/api";
import { pages } from "./routes/pages";
import { populateUserTables } from "./model/user";
import * as loaders from "./loaders";
import { RegisterRoutes } from "./build/routes";

const app = express();
loaders.init(app)

/* ==================== */

app.use("/api/v1", apiRoutes);

app.use(pages);
RegisterRoutes(app)

/* ==================== */

app.use(errorhandler())

console.log("Starting...");

const PORT = parseInt(process.env.PORT || "8080");
const HOST = process.env.HOST || "0.0.0.0";

Promise.all([
  // populateUserTables()

]).then(() => {

  app.listen(PORT, HOST, () => {
    console.log(`Server is listening on ${HOST}:${PORT}`);
    process.send && process.send('ready')
  });

})
