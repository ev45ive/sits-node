function echo(msg, error){
  return new Promise((resolve, reject)=>{        
      setTimeout(()=> {
          error? reject(error) : resolve(msg) 
      }, 2000)
  })
}

p = echo('Placki')
p.then( (v) => console.log(v))

p.then( (v) => { return 'Lubie '+v})
.then(console.log)


p3 = p.then(v => echo('I Jeszcze jedne '+v))
p3.then(console.log)


p4 = p
.then(v => echo('Super '+v, "upss.."))
.catch(err => echo('Marchewki'))
.then(v => 'Lubie '+v)
.then(console.log)
.catch(err => console.error('Try again later'))


p = echo('Placki')
p2 = echo('Ciastka').then(v => 'Lubie '+v)

Promise.all([p,p2]).then(console.log)


// Promise {<pending>}
// (2) ["Placki", "Lubie Ciastka"]

Promise.all([p,p2]).then((res) => {
  var [a,b] = res
  console.log(a,b)
})

Promise.all([p,p2]).then(([a,b]) => {
  console.log(a,b)
})

// Async Await
const a = async ()=>{   
  try{

    p = await echo('Placki')
    console.log(p);

    p2 =  'Lubie '+p
    console.log(p)


    p3 = await echo('I Jeszcze jedne '+v)
    console.log(p3)

    p4 = await  echo('Super '+v, "upss.."))
    p4 =  'Lubie '+p
    console.log(p3)

  }catch(err){    
    console.error(err)
  }
}
