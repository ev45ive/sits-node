# GIT

cd ..
git clone https://bitbucket.org/ev45ive/sits-node.git nazwa_katalogu
cd nazwa_katalogu
npm i 
npm watch:ts
npm watch:debug

# Instalcje

https://code.visualstudio.com/
code -v
1.40.1

https://nodejs.org/en/
node -v
v14.5.0

npm -v
6.14.5

https://git-scm.com/download/win
git --version
git version 2.23.0.windows.1

# GLobals

https://nodejs.org/api/globals.html

# Nodemon

npm install -g nodemon
nodemon ./src/index.js

npm install nodemon

# ES6 modules

package.json -> type = module
nodemon --experimental-modules src/index.mjs
"watch": "nodemon --experimental-modules src/index.mjs"

# TypeScript

npm i -g typescript

tsc --init
// tsconfig.json

```json
"lib": ["ES2020"],
"sourceMap": true,
"outDir": "./dist/",
"rootDir": "./src/", 
"strict": false
```

npm i typescript @types/node   @types/express

# Event Emitters
https://github.com/EventEmitter2/EventEmitter2

# Sequelize CLI
npm i -g sequelize-cli
sequelize-cli init
sequelize-cli model:generate --name Product --attributes name:string,price:number,description:string

mv models/product.js model/product.ts
tsc model/product.ts --outDir models

sequelize-cli db:migrate

sequelize-cli seed:generate --name demo-product
sequelize-cli db:seed:all

sequelize migration:generate  

# Decorators  TSOA - generaate code from annotations
https://tsoa-community.github.io/docs/getting-started.html#configuring-tsoa-and-typescript
yarn run tsoa spec-and-routes

# Process manager
npm install pm2@latest -g
https://12factor.net/

https://pm2.keymetrics.io/docs/usage/cluster-mode/